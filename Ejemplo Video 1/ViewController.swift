//
//  ViewController.swift
//  Ejemplo Video 1
//
//  Created by Francesc Roig i Feliu on 18/03/2019.
//  Copyright © 2019 Francesc Roig i Feliu. All rights reserved.
//

// Importante!! Hay que añadir el video en:
// your project root > your Target > Build Phases > Copy Bundle Resources



import UIKit
import AVKit // Se ha añadido.



class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    
    
    /*
     * Acción del botón de reproducción.
     */
    @IBAction func accionBotonPlay(_ sender: Any) {
        // Invoca la función de reproduccion con el nombre del archivo de vídeo.
        playVideo("esade.mp4")
    
    }

    
    
    /*
     * Función que reproduce video.
     */
    func playVideo(_ str_file:String) {
        guard let path = Bundle.main.path(forResource: str_file, ofType: nil) else {
            debugPrint("Video not found!")
            return
        }
        let player = AVPlayer(url: URL(fileURLWithPath: path))
        let playerController = AVPlayerViewController()
        playerController.player = player
        present(playerController, animated: true) {
            player.play()
        }
    }
    
} // Final 'ViewController'

